<?php

$currencies = [
    'uah' => [
       'name' => 'Гривна',
       'course' => 1,
    ],
    'usd' => [
       'name' => 'Доллар',
       'course' => 27.1,
    ],
    'eur' => [
       'name' => 'Евро',
       'course' => 30.2,
    ],
 ];

 $products = [
    ['title' => 'Велосипед',
     'price_val' => 24800,
     'discount_type' => 'percent',
     'discount_val' => 13,
    ],
    ['title' => 'Самокат',
     'price_val' => 10200,
     'discount_type' => 'value',
     'discount_val' => 1340,
    ],
    ['title' => 'Скейтборд',
     'price_val' => 5030,
     'discount_type' => 'percent',
     'discount_val' => 22,
    ],
    ['title' => 'Ролики',
     'price_val' => 2670,
     'discount_type' => 'value',
     'discount_val' => 430,
    ],
    ['title' => 'Коньки',
     'price_val' => 3657,
     'discount_type' => 'percent',
     'discount_val' => 17,
    ],
    ['title' => 'Сегвей',
     'price_val' => 13520,
     'discount_type' => 'percent',
     'discount_val' => 28,
    ],
    ['title' => 'Гироскутер',
    'price_val' => 8700,
    'discount_type' => 'value',
    'discount_val' => 2010,
   ],
   ['title' => 'Лыжи',
   'price_val' => 18900,
   'discount_type' => 'value',
   'discount_val' => 5100,
  ],
  ['title' => 'Сноуборд',
  'price_val' => 20800,
  'discount_type' => 'value',
  'discount_val' => 800,
 ],
 ['title' => 'Сани',
 'price_val' => 530,
 'discount_type' => 'value',
 'discount_val' => 150,
]
];


?>