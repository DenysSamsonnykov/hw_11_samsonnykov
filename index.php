<?php

session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/data/data.php';

$currency = $currencies['uah'];
if(!empty($_SESSION['currencies'])){
    $currency = $currencies[$_SESSION['currencies']];
};
$course = $currency['course'];
include_once $_SERVER['DOCUMENT_ROOT'].'/function/function.php';

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <meta name="Description" content="learning $_SESSION & Function">
    <title>Let's do this Function </title>
</head>
<body>
    <h1>Мир спортивных новинок "Всегда на ходу" приветствует Вас!</h1>
    <h2>Какая валюта вам подходит?</h2>
    <form action="sessionOnly.php" method="post" class="float-left">
        <div class="form-group mb-3">
            <select class="form-select" name="currencies">
                <option value="uah" selected>Сделайте выбор</option> 
                <option value="uah"><?= $currencies['uah']['name']; ?></option>
                <option value="usd"><?= $currencies['usd']['name']; ?></option>
                <option value="eur"><?= $currencies['eur']['name']; ?></option>
            </select>
        </div>
        <button class="btn btn-success">Готово!</button>
    </form>
    <div class="float-left">
        <table class="table table-warning table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название товара</th>
                    <th scope="col">Цена товара в ₴</th>
                    <th scope="col">Цена товара со скидкой в валюте: <?=$currency['name'];?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($products as $key => $product) : extract($product);  ?>   
                    <tr>
                        <th scope="row"><?= ++$key; ?></th> 
                        <td><?= $title; ?></td>
                        <td><?= $price_val*$currencies['uah']['course']; ?></td>
                        <td><?=convertPrice(getPriceWithDiscount($price_val, $discount_type, $discount_val), $course); ?></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
    </div>
</body>
</html>