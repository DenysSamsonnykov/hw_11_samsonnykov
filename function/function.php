<?php

function getPriceWithDiscount($price_val, $discount_type, $discount_val) {
    if($discount_type == 'value'){
        $price = $price_val - $discount_val;
        return round($price, 2);
    }elseif($discount_type == 'percent'){
        $price = $price_val*((100-$discount_val)/100);
        return round($price, 2);
    };
};
function convertPrice($price_val, $course){
    $priceEnd = round($price_val / $course, 2);
    return $priceEnd;
};

?>